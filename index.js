// console.log('hellow wo')

// [SECTION] - Parameters and Arguments
// functions in jS are lines/blocks aof codes tell ou device/app to perform a certain task

function printInput() {
	let nickname = prompt('Enter Your Nickname')

	console.log('Hi, '+nickname)
} 

// printInput()

// In some cases basics function may not be ideal

// for other cases, functions can also proccess data directly into it instead

function printName(name) {
	console.log('My Name is '+name)
}

printName('Meljohn')

function checkDivisibilityBy8(num) {
	let remainder = num % 8
	console.log(`The remainder of ${num} divided by 8 is: ${remainder}`)
	let isDivisibleBy8 = remainder === 0;
	console.log(`Is ${num} divisible by 8?`)
	console.log(isDivisibleBy8)
}

checkDivisibilityBy8(28)

function argumentFunction(){
	console.log('This function was passed as an argument before the message was printed')
}

function invokeFunction(argumentFunction) {
	argumentFunction()
}

invokeFunction(argumentFunction)

// FUNCTION - multiple parameters


function createFullName(firstName, lastName) {
	console.log(`${firstName} ${lastName}`)
}

// createFullName("Meljohn","Mangco")



function printFullName(middleName, firstName, lastName){
	console.log(`${firstName} ${middleName} ${lastName}`)
}

printFullName("Juan", "Dela", "Cruz")

function returnFullName(firstName, middleName, lastName) {
	return firstName + " " + middleName + " " + lastName
}

let completeName = returnFullName("Jeffrey", "Smith", "Bezos")

console.log(completeName)

// you can alse create a varible inside the function to contain the result and return that variable instead

function returnAddress(city, country) {
	let fullAddress = city + ", " + country
	return fullAddress
}

let myAddress = returnAddress("Cebu City", "Philippines")
console.log(myAddress)


